import sqlite3
import logging
import csv
import argparse
import os.path
from os import path

#Permet la création de logging
logging.basicConfig(filename='logfile.log', format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)
logging.info("Program Started")

# Permet la connexion à la BDD
connexion = sqlite3.connect('Auto.bd')
logging.info("Connection successful with the DataBase")
c = connexion.cursor()

# Création d'une table dans la BDD si elle existe pas
c.execute('CREATE TABLE IF NOT EXISTS Auto(nom TEXT, prenom TEXT, adresse TEXT, immat TEXT PRIMARY KEY, date_immat TEXT, marque TEXT, carrosserie TEXT, categorie TEXT, couleur TEXT, cylindre INT, denomination TEXT, energie TEXT, places INT, poids INT, puissance INT, type_variante_version TEXT, vin TEXT)')

connexion.commit()

def insertLine(row, connexion):
    print(len(row))
    donnee_BDD = [row[index] for index in range(0, 17)] 
    c.execute('''INSERT OR REPLACE INTO Auto (nom, prenom, adresse, immat, date_immat , marque, carrosserie, categorie, couleur, cylindre, denomination, energie, places, poids, puissance, 
    type_variante_version, vin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);''', donnee_BDD)

    connexion.commit()

if __name__ == "__main__":

    #Information pour le logging
    parser=argparse.ArgumentParser(description="Programme d'insertion de data d'un csv vers une BDD Sqlite.")
    parser.add_argument("cheminFichierCSV", help="Chemin vers le fichier CSV correspondant à la data à extraire", type=str)
    args = parser.parse_args()

    #Vérification existance du fichier en paramètre
    if not os.path.isfile(args.cheminFichierCSV) or not args.cheminFichierCSV.lower().endswith('.csv'):
        logging.warning("FILE ERROR") 
        logging.warning("Program brutally stopped")
        os.sys.exit(1)

    #Ouverture CSV
    with open('data.csv', 'r', newline='') as csvfile:
        logging.info("CSV file opened")

        csvreader = csv.reader(csvfile, delimiter=';')
        first_line = True

        for row in csvreader:
            if first_line == True :
                first_line = False
                logging.info("Insertion of Data started")
            else:
                insertLine(row, connexion)
        logging.info("Insertion of Data finished")
    logging.info("CSV file closed")

    # On clôture la connexion avec la BDD
    connexion.close()
    logging.info("Connection with the DataBase closed successfully")
    logging.info("Program Stopped")

