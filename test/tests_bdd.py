import unittest
import sqlite3
import logging
import csv 

from main import InsertLine

class TestBDDFunction(unittest.TestCase):

    def setUp(self):
        self.connexion = sqlite3.connect('bdd_tdd.bd')
        cursor = self.connexion.cursor()
        cursor.execute('CREATE TABLE IF NOT EXISTS Auto(nom TEXT, prenom TEXT, adresse TEXT, immat TEXT PRIMARY KEY, date_immat TEXT, marque TEXT, carrosserie TEXT, categorie TEXT, couleur TEXT, cylindre INT, denomination TEXT, energie TEXT, places INT, poids INT, puissance INT, type_variante_version TEXT, vin TEXT)')
        self.connexion.commit()

    def tearDown(self):
        self.connexion.close()
        self.connexion = None
        import os
        os.remove('bdd_tdd.bd')

    def insert_sample(self):
        pass


    def test_InsertLine(self):

        with open("test1.csv") as csvfile:
            reader = csv.reader(csvfile, delimiter='|') 
            for row in reader:
                InsertLine(row, self.connexion)
                self.connexion.commit()

        cursor = self.connexion.cursor()
        cursor.execute("SELECT * FROM Auto")
        rows_bd = cursor.fetchall()

        for row_bd in rows_bd:
            self.assertEqual(row_bd, ('a', 'd', '1', 'f', 'k', 'l', '4', 'p'))
    

    def test_UpdateLine(self):
        
        with open("test1.csv") as csvfile:
            reader = csv.reader(csvfile, delimiter='|') 
            for row in reader:
                InsertLine(row, self.connexion)
                self.connexion.commit()

        
        with open("test2.csv") as csvfile:
            reader = csv.reader(csvfile, delimiter='|') 
            for row in reader:
                InsertLine(row, self.connexion)
                self.connexion.commit()

        
        cursor = self.connexion.cursor()
        cursor.execute("SELECT * FROM Auto")
        rows_bd = cursor.fetchall()

        for row_bd in rows_bd:
            self.assertEqual(row_bd, ('jcp', 'b', 'u', 'x', 'e', 'l', '2', 'p', 'q'))